# BASE DIRECTORY / CONTEXT SHOULD BE ../ (set by docker-compose)
# The tag here should match the Meteor version of your app, per .meteor/release
FROM geoffreybooth/meteor-base:1.10.1

# Modified scripts that e.g. use Yarn instead of NPM
COPY docker-scripts/* $SCRIPTS_FOLDER/

# Copy app yarn files into container (so that the yarn step can be cached independently of app files)
COPY template/package.json template/yarn.lock $APP_SOURCE_FOLDER/

RUN mkdir -p $APP_SOURCE_FOLDER/node_modules && bash $SCRIPTS_FOLDER/build-app-yarn-dependencies.sh
# the node_modules part is a try of a workaround for https://gitlab.com/tennoxlab/food-labeler/-/jobs/474927966

# Copy app source into container
COPY template/ $APP_SOURCE_FOLDER/

RUN bash $SCRIPTS_FOLDER/build-meteor-bundle.sh


# Rather than Node 12 latest (Alpine), you can also use the specific version of Node expected by your Meteor release, per https://docs.meteor.com/changelog.html
FROM node:12-alpine

ENV APP_BUNDLE_FOLDER /opt/bundle
ENV SCRIPTS_FOLDER /docker

# Install OS build dependencies, which we remove later after we’ve compiled native Node extensions
RUN apk --no-cache --virtual .build-deps add \
		build-base \
		python3 \
	# And runtime dependencies, which we keep
	&& apk --no-cache add \
		bash \
		ca-certificates

# Copy in scripts
COPY --from=0 $SCRIPTS_FOLDER $SCRIPTS_FOLDER/
# Copy in app bundle
COPY --from=0 $APP_BUNDLE_FOLDER/bundle $APP_BUNDLE_FOLDER/bundle/

# @manu changed from original - don't rebuild all from source - only bcrypt
RUN bash $SCRIPTS_FOLDER/build-meteor-npm-dependencies.sh \
    && cd $APP_BUNDLE_FOLDER/bundle/programs/server/npm \
	&& npm rebuild --build-from-source bcrypt \
    && apk del .build-deps

# Start app
ENTRYPOINT ["/docker/entrypoint.sh"]
CMD ["node", "main.js"]
