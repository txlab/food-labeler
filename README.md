# Food labeler

#### Installation

1. clone this repository:
2. cd into the 'template' folder:
3. Install from yarn: `yarn`
4. ~~Create a link to quasar in the /imports directory:~~ *(should exist already)*  
    `ln -s ../node_modules/quasar imports`  
    (If you are doing this on *Windows* the link command is:  
    `mklink /D "imports\quasar" "..\node_modules\quasar\"`
5. ~~Create a link to the quasar-extras .woff files in the /public directory.~~ *(should exist already)*  
    This makes the material-icons stuff appear:  
    `ln -s ../node_modules/@quasar/extras/material-icons/web-font public`  
    (If you are doing this on *Windows* the link command is:  
    `mklink /D "public\web-font" "..\node_modules\@quasar\extras\material-icons\web-font"`
    There is a good explanation on StackOverflow of why there is a problem with the .woff files
    https://stackoverflow.com/questions/34133808/webpack-ots-parsing-error-loading-fonts  
6. run meteor dev server (still inside the template folder): `meteor run`

It should eventually say:
App running at: http://localhost:3000/

Point your desktop browser to that address.


### Built on [Quasar Meteor Starter Kit](https://github.com/quasarframework/quasar-template-meteor) 

This project uses the Akryum projects to get Meteor working with vuejs and quasar-framework.
The most useful page to consult is:
https://github.com/meteor-vue/vue-meteor-tracker

This is the main page for all the Meteor/Vuejs projects:
https://github.com/meteor-vue/vue-meteor

#### Note:
The 'template' folder is a necessary part of the quasar framework.
However all meteor commands should only be run once you have moved inside the 'template' folder.
This extra 'template' folder is there because quasar-framework requires it. 
However, Quasar-framework uses Webpack for its builds, but Meteor has its own build system.
This means that it is unlikely that you will be able to use *quasar cli*, because it all leads up to a quasar build using webpack.

To use Meteor, just cd into the 'template' folder and run all your usual meteor commands from there.



