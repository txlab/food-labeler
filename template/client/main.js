
//quasar framework is based on vuejs
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLodash from 'vue-lodash'

import Quasar, { ClosePopup, Notify, Dialog } from 'quasar'
// import materialOutlinedIcons from 'quasar/icon-set/material-icons-outlined'

// import lodash from 'lodash'
import Croppa from 'vue-croppa'
import 'vue-croppa/dist/vue-croppa.css'

import { routes } from './routes'
import AppLayout from '/imports/ui/AppLayout.vue'

// _ = lodash

// // name is optional
// Vue.use(VueLodash, { lodash })

Vue.use(VueRouter)
const router = new VueRouter({
    routes, // short for `routes: routes`
})

Vue.use(Croppa)

//App start
Meteor.startup(() => {
    Vue.use(Quasar, {
        plugins: { Notify, Dialog },
        directives: { ClosePopup },
        // iconSet: materialOutlinedIcons,
    })
    new Vue({
        router: router,
        render: h => h(AppLayout),
    }).$mount('app')
})

