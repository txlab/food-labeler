// Components
import EditToolBar from "../imports/ui/edit/EditToolBar";
import HomeToolBar from "../imports/ui/HomeToolBar";
import Edit from "../imports/ui/edit/Edit";
import Home from "../imports/ui/List";
import Help from "../imports/ui/Help";
import Print from "../imports/ui/Print"

export const routes = [
    {
        path: '/',
        name: 'home',
        components: {
            default: Home,
            toolbar: HomeToolBar
        },
    },
    {
        path: '/edit/:labelKey',
        name: 'edit',
        props: {default: true, toolbar: true},
        components: {
            default: Edit,
            toolbar: EditToolBar
        },
    },
    {
        path: '/print/:labelKey',
        name: 'print',
        props: true,
        component: Print,
    },
    {
        path: '/help',
        name: 'help',
        component: Help,
    },
];
