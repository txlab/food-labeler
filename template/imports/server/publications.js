import {Meteor} from 'meteor/meteor'
import {Labels} from '../lib/collections.js'

Meteor.publish('labels', function () {
    return Labels.find({}, {
        fields: {image: 0},
    })
})

Meteor.publish('labelByKey', function (key) {
    return Labels.find({key: key})
})
Meteor.publish('labelsByKeys', function (keys) {
    return Labels.find({key: {$in: keys}})
})
