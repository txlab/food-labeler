export const TYPE_OPTIONS = [
    { label: 'Small', value: 'small', image: false, icon: 'crop_7_5' },
    { label: 'Medium', value: 'medium', image: true, icon: 'crop_landscape' },
    { label: 'Large', value: 'large', image: true, icon: 'crop_din' }
]

export const CATEGORY_ICONS = [
    { key: 'algae', label: 'Algae' },
    { key: 'bread', label: 'Bread' },
    { key: 'cereal', label: 'Cereal' },
    { key: 'cheese', label: 'Cheese' },
    { key: 'cream', label: 'Cream' },
    { key: 'dried_fruit', label: 'Dried Fruit' },
    { key: 'drinks', label: 'Drinks' },
    { key: 'eggs', label: 'Eggs' },
    { key: 'flour', label: 'Flour' },
    { key: 'fruit', label: 'Fruit' },
    { key: 'honey', label: 'honey' },
    { key: 'herbs-spices', label: 'Herbs Spices' },
    { key: 'non-food', label: 'Non Food' },
    { key: 'nuts', label: 'Nuts' },
    { key: 'oil-vinegar', label: 'Oil Vinegar' },
    { key: 'olives', label: 'Olives' },
    { key: 'pickles', label: 'Pickles' },
    { key: 'processed-food', label: 'Processed Food' },
    { key: 'pulses', label: 'Pulses' },
    { key: 'salt', label: 'Salt' },
    { key: 'seeds', label: 'Seeds' },
    { key: 'tea', label: 'Tea' },
    { key: 'vegetables', label: 'Vegetables' },
]

export const IMPACT_CATEGORIES = [
    { key: 'high', label: 'High', color: 'red', textColor: 'white', gradient: 'linear-gradient(270deg, rgba(235,94,80,255) 0%, rgba(231,52,63,1) 100%)' },
    { key: 'medium', label: 'Medium', color: 'orange', textColor: 'white', gradient: 'linear-gradient(270deg, rgba(222,120,74,255) 0%, rgba(243,150,85,1) 100%)' },
    { key: 'low', label: 'Low', color: 'yellow-7', textColor: 'white', gradient: 'linear-gradient(270deg, rgba(249,180,92,255) 0%, rgba(254,208,97,1) 100%)' },
    { key: 'great', label: 'Great', color: 'green', textColor: 'white', gradient: 'linear-gradient(270deg, rgba(57,135,86,255) 0%, rgba(84,165,89,1) 100%)' },
]