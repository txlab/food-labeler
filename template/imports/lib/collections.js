
import { Mongo } from 'meteor/mongo';

const Labels = new Mongo.Collection('labels');
if (Meteor.isServer) {
    Labels._ensureIndex({key: 1}, {unique: 1});
}

export {Labels};