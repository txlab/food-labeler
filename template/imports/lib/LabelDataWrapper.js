import {IMPACT_CATEGORIES, TYPE_OPTIONS} from "../ui/constants"
import { find, omitBy, has, assign } from 'lodash'

export const Defaults = {
    type: 'medium',
    impact: 'medium'
}
const Extensions = {
    typeInfo() {
        return find(TYPE_OPTIONS, {value: this.type})
    },
    impactInfo() {
        return find(IMPACT_CATEGORIES, {key: this.impact})
    },
    data() {
        return omitBy(this, (val, key) => has(Extensions, key))
    }
}

export default (data) => {
    if (data === undefined || data === null) {
        console.warn(`LabelDataWrapper called on non-object:`, data)
    } else {
        // _.defaults(data, Defaults) - only for e.g. migration
        assign(data, Extensions)
    }
    return data
}